import { ProductoService } from './../services/productos.service';
import { Component } from '@angular/core';
import Productos from "../data/productos.json";
import { Router } from '@angular/router';

@Component({
  selector: 'diy',
  templateUrl: 'diy.page.html',
  styleUrls: ['diy.page.scss'],
})
export class DiyPage {

  private listaProductos;
  selectedProduct;
  productId;


  constructor(private router: Router, private _productoService: ProductoService) {}

  ngOnInit(){
    this.listaProductos = this._productoService.getProductos();  
  }
  abrirCustomizerDesdeServicio(producto){
    this._productoService.setCustomizer(producto);
   /* console.log("desde DIY",this.selectedProduct.id);
    this.productId = this.selectedProduct.id;
    this.router.navigateByUrl('/customizer/',this.productId)
     this.router.navigate(['/customizer', this.selectedProduct.id]); 
    console.log("el producto seleccionado en DIY es: ", this.selectedProduct);
    */
  }
}
