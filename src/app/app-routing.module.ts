/* import { ResolverService } from './services/resolver.service'; */
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'diy', pathMatch: 'full' },
  { path: 'customizer', redirectTo: 'diy', pathMatch: 'full' },
  { path: 'diy', loadChildren: () => import('./diy/diy.module').then( m => m.DiyPageModule)},
  /* { path: 'customizer', loadChildren: './customizer/customizer.module#CustomizerPageModule' }, */
  { path: 
    'customizer/:id',
    /* resolve: {
      special: ResolverService
    }, */
    loadChildren: './customizer/customizer.module#CustomizerPageModule' 
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
