import { NgModule } from '@angular/core';
import { CustomizerPage } from './customizer/customizer.page';

@NgModule({
    declarations: [CustomizerPage],
    exports: [CustomizerPage]
})
export class ComponentsModule{}