import { ActivatedRoute, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import Productos from "../data/productos.json";

@Injectable({
    providedIn: 'root'
})
export class ProductoService {
  
  private listaProductos = Productos;
  selectedProduct;
  productId;

  constructor(private activatedRoute: ActivatedRoute, private router:Router) {

  }

  ngOnInit() { 

  }

  getProductos(){
    console.log("mostrando lista de productos desde el servicio", this.listaProductos);
    return this.listaProductos = Productos;
  }

  setCustomizer(producto){
    this.selectedProduct = producto;
    console.log("el producto seleccionado en el Servicio es: ", this.selectedProduct);
    console.log("Desde Servicio",this.selectedProduct.id);
    this.productId=this.selectedProduct.id;
    this.getSelectedProduct();
    /* this.productId = this.selectedProduct.id; */
    this.router.navigate(['/customizer', this.productId]);
    
  }
  getSelectedProduct(){
    return this.selectedProduct;
  }
}