import { ProductoService } from './../services/productos.service';
import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "customizer",
  templateUrl: "./customizer.page.html",
  styleUrls: ["./customizer.page.scss"]
})
export class CustomizerPage implements OnInit {

  selectedProduct;
  id;

  tallaSelected="";
  colorBaseSelected;
  estampadoBaseSelected="";
  colorVolantesSelected="";
  estampadoVolantesSelected="";
  trianguloSSelected="";
  trianguloISelected="";
  colorEscoteSlected="";
  estampadoEscoteSelected="";
  colorLetrasSelected="";
  colorCinta1Selected="";
  colorCinta2Selected="";
  colorCinta3Selected="";
  colorCitaD1Selected="";
  colorCitaD2Selected="";
  colorCitaD3Selected="";
  colorCitaI1Selected="";
  colorCitaI2Selected="";
  logoCorpSelected="";
  colorRomboSelected="";
  colorCirculoSelected="";
  combinacionDegradadoSelected="";
  colorBordadoSelected="";

  codigoCustom="";
/* +this.estampadoBaseSelected+this.colorVolantesSelected+this.estampadoVolantesSelected+this.trianguloSSelected+this.trianguloISelected+this.colorEscoteSlected+this.estampadoEscoteSelected+this.colorLetrasSelected+this.colorCinta1Selected+this.colorCinta2Selected+this.colorCinta3Selected+this.colorCitaD1Selected+this.colorCitaD2Selected+this.colorCitaD3Selected+this.colorCitaI1Selected+this.colorCitaI2Selected+this.logoCorpSelected+this.colorRomboSelected+this.colorCirculoSelected+this.combinacionDegradadoSelected+this.colorBordadoSelected */

 /*  codigoPersonalizado; */

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private _productoService: ProductoService) {
  }

  ngOnInit() {
    this.selectedProduct = this._productoService.getSelectedProduct();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.tallaSelected = this.selectedProduct.size[0];
    this.colorBaseSelected = this.selectedProduct.baseColor[0];
/*     if (this.route.snapshot.data['special']){
      this.id = this.route.snapshot.data['special'];
    } */
  }
//Base principal
copiarCodigo(code){
  code.select();
  document.execCommand('copy');
  code.setSelectionRange(0,0);
  this.codigoCustom = code;
  /*alert("Tu codigo es: "+ this.codigoCustom);*/
  console.log("el codigo es: ",this.codigoCustom);
}
cambiarTalla(talla){
    this.tallaSelected = talla;
    console.log("La talla escogida es: ", this.tallaSelected);
  }
  cambiarColorbase(baseColor){
    this.colorBaseSelected = baseColor;
    this.selectedProduct.imgF = this.colorBaseSelected.imgF;
    this.selectedProduct.imgB = this.colorBaseSelected.imgB;
    console.log("El Color base escogido es: ", this.colorBaseSelected);
  }
//Base principal
//Estampados
  cambiarEstampadoBase(estampadoBase){
    this.estampadoBaseSelected = estampadoBase;
    console.log("El Estampado base escogido es: ", this.estampadoBaseSelected);
  }
//EstampadosEnd
//Producto3
  cambiarColorVolantes(colorVolantes){
    this.colorVolantesSelected = colorVolantes;
  }
  cambiarEstampadoVolantes(estampadoVolantes){
    this.estampadoVolantesSelected = estampadoVolantes;
  }
//Producto4
  cambiarTS(){

  }
  cambiarTI(){

  }
//Producto5
  cambiarColorEscote(){

  }
  cambiarEstampadoEscote(){

  }
//Producto7
  cambiarColorLetras(){

  }
//Producto8 y 11
  cambiarColorCita1(){

  }
  cambiarColorCita2(){
    
  }
  cambiarColorCita3(){
    
  }
//Producto9
  cambiarColorCitaD1(){

  }
  cambiarColorCitaD2(){
    
  }
  cambiarColorCitaD3(){
    
  }
  cambiarColorCitaI1(){

  }
  cambiarColorCitaI2(){
    
  }
  cambiarColorCitaI3(){
    
  }

  logoCorporativo(){

  }
//Producto10
  cambiarColorRombo(){

  }
  cambiarColoCirculo(){

  }
//Producto12
  cambiarCombinacionDegrado(degradadoCuadrado){
    this.combinacionDegradadoSelected = degradadoCuadrado;
    console.log("El degradado escogido es: ", this.combinacionDegradadoSelected);
  }
//Producto13
  cambiarColoresBordados(){

  }
}
